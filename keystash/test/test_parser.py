# -*- coding:utf-8 -*-
# $Id$
#
# Copyright (C) 2003  Andrey Lebedev <andrey@micro.lt>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import unittest

from keystash import parser

class ParserTestCase(unittest.TestCase):

    pattern = '(^|.*\s)Username/Password: (?P<user>.*?)/(?P<password>.*?)($|\s)'
    cases = [
        {"text": 'Username/Password: actualusername/actualpassword',
        "match": {'password': 'actualpassword', 'user': 'actualusername'}},

        {"text": '''This is a first line
        Username/Password: longusername/longpassword  asd
        Some other line''',
        "match": {'password': 'longpassword', 'user': 'longusername'}},

        {"text": 'USERNAME/PASSWORD: the username/!@#%^^&*(',
        "match": {'password': '!@#%^^&*(', 'user': 'the username'}},
    ]

    ml_pattern = '''^
    Username: (?P<user>.*?)
    Password: (?P<password>.*?)
(?P<description>.*?)
$'''
    ml_cases = [
        {"text": '''
    Username: mlusername
    Password: mlpassword

''',
        "match": {'password': 'mlpassword', 'user': 'mlusername'}},

        {"text": '''
    Username: ''' + '''
    Password: mlpassword2

    This is a multiline
    description
''',

        "match": {'password': 'mlpassword2', 'description': '''This is a multiline
    description'''}}
    ]

    def test_parse(self):
        for pattern, cases in [(self.pattern, self.cases), (self.ml_pattern, self.ml_cases)]:
            for case in cases:
                match = parser.parse(pattern, case["text"])
                self.assertEqual(match, case["match"])

    def test_regularise(self):
        cases = [
            ("{~.*}", "(^|.*\s).*($|\s)"),
            ("{~test.+}", '(^|.*\s)test.+($|\s)'),

            ("{the}{test}", '(^|.*\s)(?P<the>.*?)(?P<test>.*?)($|\s)'),
            ("""{hello}
            {~(test)+}
            {}
            {welcome}
            """,
            """(^|.*\s)(?P<hello>.*?)
            (test)+
            .*
            (?P<welcome>.*?)
            ($|\s)"""),
        ]
        for pattern, regexp in cases:
            res = parser.regularise(pattern)
            self.assertEqual(res, regexp)

    def test_parser(self):
        pattern = "Username/Password: {user}/{password}"
        expr = parser.regularise(pattern)
        for case in self.cases:
            match = parser.parse(expr, case["text"])
            self.assertEqual(match, case["match"])

    def test_parseMessage(self):
        texts = ["username/password: actualuser/actualpassword",
        """Username : actualuser
        Password    : actualpassword""",
        "user: actualuser pass:actualpassword"
        ]
        result = {'password': 'actualpassword', 'user': 'actualuser'}
        for text in texts:
            dict = parser.parseMessage(text, parser.patterns)
            self.assertEqual(dict, result)


def suite():
    return unittest.makeSuite(ParserTestCase, 'test')

if __name__ == "__main__":
    unittest.main()

# -*- coding:utf-8 -*-
# $Id$
#
# Copyright (C) 2003  Andrey Lebedev <andrey@micro.lt>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

""" Password Database """

from password_tree import PasswordTree

class DatabaseNotExist(IOError):
    pass

class DatabaseWrongFormat(DatabaseNotExist):
    pass

class PasswordDatabase:
    """ Base class for password databases.
    Real databases should be implemented in plugins
    """

    def __init__(self, **args):
        self._pass_tree = PasswordTree()

    def open(self, password = ""):
        """ Open database from external source """
        pass

    def save(self, fname=""):
        """ Save database to external source """
        pass

    def create(self, password, fname=""):
        """Create new password database"""
        pass

    def changePassword(self, newpassword):
        """Change password for database"""
        pass

    def getTree(self):
        return self._pass_tree

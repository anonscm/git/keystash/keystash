# -*- coding:utf-8 -*-
# $Id$
#
# Copyright (c) 2009 Thorsten Glaser <t.glaser AT tarent.de>
#
# Provided that these terms and disclaimer and all copyright notices
# are retained or reproduced in an accompanying document, permission
# is granted to deal in this work without restriction, including un‐
# limited rights to use, publicly perform, distribute, sell, modify,
# merge, give away, or sublicence.
#
# This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
# the utmost extent permitted by applicable law, neither express nor
# implied; without malicious intent or gross negligence. In no event
# may a licensor, author or contributor be held liable for indirect,
# direct, other damage, loss, or other issues arising in any way out
# of dealing in the work, even if advised of the possibility of such
# damage or existence of a defect, except proven that it results out
# of said person’s immediate fault when using the work as intended.

__all__ = ['kwallet_available', 'kwallet_get', 'kwallet_put']

import subprocess


def kwallet_available():
	"""Return True if kwalletcli can be run, False otherwise."""
	try:
		p = subprocess.Popen(["kwalletcli", "-qV"])
	except Exception:
		return False
	p.communicate()
	if p.returncode == 0:
		return True
	return False


def kwallet_get(folder, entry):
	"""Retrieve a passphrase from the KDE Wallet via kwalletcli.

	Arguments:
	• folder: The top-level category to use (normally the programme name)
	• entry: The key of the entry to retrieve

	Returns the passphrase as unicode, False if it cannot be found,
	or None if an error occured.

	"""
	p = subprocess.Popen(["kwalletcli", "-q", "-f", folder.encode('utf-8'),
	 "-e", entry.encode('utf-8')], stdout=subprocess.PIPE)
	pw = p.communicate()[0]
	if p.returncode == 0:
		return unicode(pw.decode('utf-8'))
	if p.returncode == 1 or p.returncode == 4:
		# ENOENT
		return False
	# error
	return None


def kwallet_put(folder, entry, passphrase):
	"""Store a passphrase into the KDE Wallet via kwalletcli.

	Arguments:
	• folder: The top-level category to use (normally the programme name)
	• entry: The key of the entry to store
	• passphrase: The value to store

	Returns True on success, False otherwise.

	"""
	p = subprocess.Popen(["kwalletcli", "-q", "-f", folder.encode('utf-8'),
	 "-e", entry.encode('utf-8'), "-P"], stdin=subprocess.PIPE)
	p.communicate(passphrase.encode('utf-8'))
	if p.returncode == 0:
		return True
	return False

# -*- coding:utf-8 -*-
# $Id$
#
# Copyright © 2009 Thorsten Glaser <t.glaser@tarent.de>
#
# Provided that these terms and disclaimer and all copyright notices
# are retained or reproduced in an accompanying document, permission
# is granted to deal in this work without restriction, including un‐
# limited rights to use, publicly perform, distribute, sell, modify,
# merge, give away, or sublicence.
#
# This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
# the utmost extent permitted by applicable law, neither express nor
# implied; without malicious intent or gross negligence. In no event
# may a licensor, author or contributor be held liable for indirect,
# direct, other damage, loss, or other issues arising in any way out
# of dealing in the work, even if advised of the possibility of such
# damage or existence of a defect, except proven that it results out
# of said person’s immediate fault when using the work as intended.

__all__ = ['rng_init', 'rng_done', 'rng_get', 'rng_put']

import os
import stat
import sys

from Crypto.Util.randpool import RandomPool

try:
    from OpenSSL import rand
    _have_openssl = True
except Exception:
    _have_openssl = False


class Arc4Rng(object):
    """RNG using /dev/arandom"""

    def __str__(self):
        """return the name of the RNG and additional info as string"""
        return "arandom (not file backed)"

    def __init__(self, poolfile):
        self._f = open("/dev/arandom", "rb")
        try:
            self._w = open("/dev/wrandom", "wb")
            self._w.write(self._f.read(1))
        except Exception:
            self._w = None

    def lostir(self):
        """low-effort stir method"""
        pass

    def fn_init(self):
        """read poolfile"""
        pass

    def fn_done(self):
        """write poolfile"""
        pass

    def fn_get(self, num):
        """get entropy"""
        return self._f.read(num)

    def fn_put(self, s):
        """put entropy"""
        if self._w:
            try:
                self._w.write(s)
            except Exception:
                self._w = None


class FileRng(object):
    """Poolfile-backed RNG"""

    _strans_01 = 512    # minimum to consider seeded
    _strans_12 = 1024   # consider “seeded enough”
    _sget_0 = 1024      # get a large chunk
    _sget_1 = 128       # get a small chunk

    def __str__(self):
        """return the name of the RNG and additional info as string"""
        return "FileRNG (backed by '%s')" % self._poolfile

    def __init__(self, poolfile):
        self._poolfile = poolfile
        try:
            os.chmod(self._poolfile, stat.S_IREAD | stat.S_IWRITE)
        except Exception:
            pass
        self._rp = RandomPool(numbytes=1024)
        self._status = 0

    def lostir(self):
        """low-effort stir method"""
        self._rp.add_event()

    def p_read(self):
        """internal: read RandomPool"""
        self.lostir()
        try:
            f = open(self._poolfile, "rb")
            self.lostir()
            data = f.read()
            self.lostir()
            self._rp.stir(data)
            if self._status < 1 and len(data) >= 128:
                self._status = 1
            self._rp._updateEntropyEstimate(len(data) * 7.9)
            f.close()
        except Exception:
            pass

    def p_save(self):
        """internal: save RandomPool"""
        self._rp.stir_n()
        try:
            f = open(self._poolfile, "wb")
            self.lostir()
            try:
                os.chmod(self._poolfile, stat.S_IREAD | stat.S_IWRITE)
                self.lostir()
            except Exception:
                pass
            f.write(self._rp._randpool.tostring())
            f.close()
            self._rp.stir()
        except Exception:
            pass
        self.lostir()

    def _os_get(self, nbits):
        """internal: get entropy"""
        self._rp.randomize(nbits)

    def _check(self, isread=False):
        """internal: check if there is enough entropy"""
        if self._status > 1:
            return
        if self._status < 1:
            nbits = self._sget_0
        else:
            nbits = self._sget_1
        self._os_get(nbits)
        if self._rp.entropy >= self._strans_12:
            self._status = 2
        elif self._rp.entropy >= self._strans_01:
            self._status = 1

    def fn_init(self):
        """read poolfile"""
        self.p_read()
        self._check(True)
        self.lostir()

    def fn_done(self):
        """write poolfile"""
        self.p_write()

    def fn_get(self, num):
        """get entropy"""
        self._check()
        self.lostir()
        rv = self._rp.get_bytes(num)
        self.lostir()
        return rv

    def fn_put(self, s):
        """put entropy"""
        self._rp.stir(s)
        # assume s is binary, 5 bit per byte (0.625%)
        self._rp._updateEntropyEstimate(len(s) * 5)


class UrndRng(FileRng):
    """Poolfile-backed RNG seeded by os.urandom"""

    _strans_01 = 1024   # minimum to consider seeded
    _strans_12 = 1280   # consider “seeded enough”
    _sget_0 = 768       # get a large chunk
    _sget_1 = 64        # get a small chunk

    def __str__(self):
        """return the name of the RNG and additional info as string"""
        return "urandom (seedfile '%s')" % self._poolfile

    def _os_get(self, nbits):
        """internal: get entropy"""
        try:
            d = os.urandom(nbits / 8)
            estimate = len(d) * 7.9
        except Exception, e:
            d = str(e)
            estimate = 4
        nbits -= estimate
        self._rp.stir(d)
        self._rp._updateEntropyEstimate(estimate)
        if nbits > 30:
            self._rp.randomize(nbits)


class UrndTest(object):
    """Test class for os.urandom detection"""

    def __str__(self):
        """return the name of the RNG and additional info as string"""
        return "urandom"

    def fn_get(self, num):
        """get entropy"""
        return os.urandom(num)


class OpenRng(FileRng):
    """Poolfile-backed RNG seeded by py-OpenSSL"""

    def __str__(self):
        """return the name of the RNG and additional info as string"""
        return "OpenSSL (seedfile '%s')" % self._poolfile

    def __init__(self, poolfile):
        FileRng.__init__(self, poolfile)
        for f in (os.getenv("EGD_PATH"),
          os.path.join(os.path.expanduser('~'), '.gnupg', 'entropy'),
          "/var/run/egd-pool", "/dev/egd-pool", "/etc/egd-pool",
          "/etc/entropy"):
            if f is not None and len(f) > 0 and os.path.exists(f):
                try:
                    self._rp.add_event(str(rand.egd(f)))
                except Exception:
                    pass
        self._rp.add_event(str(rand.status()))

    def lostir(self):
        """low-effort stir method"""
        try:
            rand.screen()
        except Exception:
            pass
        FileRng.lostir(self)

    def _check(self, isread=False):
        """internal: shuffle py-OpenSSL with py-Crypto"""
        if not isread:
            self.p_read()
        self._osslwrite()
        self.p_read()

    def _osslwrite(self):
        rand.seed(self._rp._randpool.tostring())
        rand.write_file(self._poolfile)

    def fn_put(self, s):
        """put entropy"""
        rand.add(str(s), len(s) * 5)

    def fn_done(self):
        """write poolfile"""
        self._osslwrite()


def try_rng():
    try:
        t1 = _rng.fn_get(1)
        t2 = _rng.fn_get(1)
    except Exception, e:
        t1 = None
        print >>sys.stderr, "Not using RNG %s: %s" % (_rng, e)
    if t1 is not None and t2 is not None and \
      len(t1) == 1 and len(t2) == 1 and t1 != t2:
        return True
    return False

_rng = None
pf = os.getenv("RANDFILE")
if pf is None or not os.path.exists(pf):
    pf = os.path.join(os.path.expanduser('~'), '.etc', 'keystash',
      'randseed.bin')
if os.path.exists("/dev/arandom"):
    _rng = Arc4Rng(pf)
    if not try_rng():
        _rng = None
if _rng is None and _have_openssl:
    try:
        _rng = OpenRng(pf)
        if not try_rng():
            _rng = None
    except Exception:
        _rng = None
if _rng is None:
    _rng = UrndTest()
    if try_rng():
        _rng = UrndRng(pf)
    else:
        _rng = None
if _rng is None:
    _rng = FileRng(pf)
print >>sys.stderr, "Using RNG: %s" % _rng


def rng_init():
    """initialise RNG, read pool file"""
    _rng.fn_init()

def rng_done():
    """write pool file to disc"""
    _rng.fn_done()

def rng_get(nbytes):
    """get 「nbytes」 of entropy"""
    return _rng.fn_get(nbytes)

def rng_put(s):
    """put 「s」 into the RNG pool"""
    _rng.fn_put(s)

# -*- coding:utf-8 -*-
# $Id$
#
# Copyright (C) 2003  Andrey Lebedev <andrey@micro.lt>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

''' KED Password Manager - simple to use, extensible and secure password
manager.

This module contains frontends implementations.
Supported frontends::

    * cli - Command Line Interface frontend

    * gtk - GTK-2 frontend'''

def frontendFactory(frontend):
    if frontend == 'cli':
        from keystash.frontends.cli import Application
        return Application()
    elif frontend == 'gtk':
        from keystash.frontends.gtk.app import Application
        return Application()
    else:
        raise ValueError, '''Frontend "%s" is not supported.
Supported frontends are:
    cli: Command line interface;
    gtk: GTK2 graphical interface;''' % frontend

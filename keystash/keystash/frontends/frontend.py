# -*- coding:utf-8 -*-
# $Id$
#
# Copyright (C) 2003  Andrey Lebedev <andrey@micro.lt>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

'''Ked Password Manager frontend abstraction.'''

import base64
import os
from shutil import copyfile
import sys

from keystash.config import Configuration
from keystash.exceptions import WrongPassword
from keystash.rng import *
from keystash.plugins.kwalletbinding import *
from keystash.plugins.pdb_figaro import PDBFigaro
from keystash.passdb import DatabaseNotExist, DatabaseWrongFormat

class Frontend:

    conf = None  # Configuration object
    has_wallet = True

    def mainLoop(self):
        '''Main loop of frontend application here.'''
        raise NotImplementedError

    def showMessage(self, message):
        '''Display an information message'''
        raise NotImplementedError

    def pwgen(self):
        """Generate a random password"""
        return base64.b64encode(rng_get(6), ",.")

    def storeMasterPW(self, fn, pw):
        """Store the Master Password in the KDE Wallet"""
        if self.conf.options["save-pw"] and self.has_wallet:
            if not kwallet_put("keystash", "v1:file:%s" % fn, pw):
                self.has_wallet = False

    def openDatabaseDialog(self):
        raise NotImplementedError

    def openDatabase(self):
        """Open database and prompt for password if necessary

        Sets self.pdb variable, opens a database and prompts for
        password if necessary using the openDatabaseDialog method.
        If no database exists yet, create an empty one, asking the
        user for necessary information (password).

        """
        db_file = os.path.expanduser(self.conf.options['db-file'])
        fpm_file = os.path.expanduser(self.conf.options['fpm-file'])

        #tries = ( (db_file, PDBKeystash), (fpm_file, PDBFigaro) )
        tries = ( (fpm_file, PDBFigaro), )

        for (fn, cn) in tries:
            try:
                if not os.path.exists(fn):
                    continue

                initpw = ""
                if self.conf.options["save-pw"] and self.has_wallet:
                    initpw = kwallet_get("keystash",
                      "v1:file:%s" % fn)
                    if initpw is None:
                        self.has_wallet = False
                    if not initpw:
                        initpw = ""

                try:
                    self.pdb = cn(filename=fn)
                    self.pdb.open(initpw)
                    self.storeMasterPW(fn, initpw)
                    return
                except DatabaseNotExist:
                    continue
                except DatabaseWrongFormat:
                    continue
                except WrongPassword:
                    self.openDatabaseDialog()
                    return
            except Exception, e:
                print "Cannot open PDB %s:" % fn
                print e
                sys.exit(1)

        #self.pdb = PDBKeystash(filename=db_file)
        self.pdb = PDBFigaro(filename=fpm_file)
        initpw = self.createNewDatabase()
        if initpw is None:
            sys.exit(1)
        self.pdb.open(initpw)
        self.storeMasterPW(self.pdb.filename, initpw)

    def run(self):
        '''Run frontnend program

        Do common initialisation stuff common to all frontends,'''

        self.has_wallet = kwallet_available()

        # Open configuration
        self.conf = Configuration()
        self.conf.open()
        # Open database
        try:
            self.openDatabase()
        except (EOFError, KeyboardInterrupt):
            print
            print "Good bye."
            sys.exit(1)
        if not self.pdb.native:
            # Do backup
            backupfile = self.pdb.default_db_filename+'.kedpm.bak'
            self.showMessage("""WARNING! KedPM has detected original FPM password database.
Backing it up to %s
""" % backupfile)
            copyfile(self.pdb.default_db_filename, backupfile)

        self.mainLoop()

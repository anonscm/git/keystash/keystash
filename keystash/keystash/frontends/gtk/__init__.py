# -*- coding:utf-8 -*-
# $Id$

import pygtk
pygtk.require("2.0");

import gtk.glade
gtk.glade.bindtextdomain('keystash', './po')
gtk.glade.textdomain('keystash')

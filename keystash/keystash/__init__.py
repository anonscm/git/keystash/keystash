# -*- coding:utf-8 -*-
# $Id$
#
# Copyright (C) 2003  Andrey Lebedev <andrey@micro.lt>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

''' KED Password Manager

Simple to use, extensible and secure password manager
'''

import os

__version__ = "0.01β"

data_files_dir = None

def setupPrefix():
    """Figure out base location where keystash data files were installed by examining
    __file__ value.

    On UNIX data files shoud be stored in <prefix>/share/keystash directory."""

    global data_files_dir
    prefix_dir = __file__
    for i in range(5):
        prefix_dir, f = os.path.split(prefix_dir)
    data_files_dir = os.path.join(prefix_dir, 'share', 'keystash')
    if not os.access(data_files_dir, os.F_OK):
        # We are in the distribution dir
        data_files_dir = "."

if data_files_dir is None:
    setupPrefix()

# Install gettext _() function
import gettext
gettext.install('keystash', './po', unicode=True)

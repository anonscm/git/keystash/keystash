#!/usr/bin/env python
# -*- coding:utf-8 -*-
# $Id$
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from distutils.core import setup
from keystash import __version__
import os, sys
from glob import glob

# patch distutils if it can't cope with the "classifiers" keyword
# that's support for python < 2.3
from distutils.dist import DistributionMetadata
if not hasattr(DistributionMetadata, 'classifiers'):
    DistributionMetadata.classifiers = None
    DistributionMetadata.download_url = None


def main():

    long_description = """Ked Password Manager helps to manage large amounts of
passwords and related information and simplifies tasks of searching and
entering password data.

Ked-PM written as extensible framework, which allows to plug in custom password
database back-ends and custom user interface front-ends. Currently only Figaro
PM back-end. To control KedPM user can choose between  CLI and GTK2 based GUI
front-ends."""

    setup(
        name="keystash",
        version=__version__,
        description="Ked Password Manager",
        long_description = long_description,
        author="Andrey Lebedev",
        author_email="andrey@users.sourceforge.net",
        license="GPL",
        platforms="POSIX",
        url="http://kedpm.sourceforge.net/",
        packages=['keystash', 'keystash.plugins', 'keystash.frontends', 'keystash.frontends.gtk'],
        scripts=['scripts/keystash'],
        data_files=[(os.path.join('share', 'keystash'), ['AUTHORS', 'COPYING', 'INSTALL']),
            (os.path.join('share', 'keystash', 'glade'),
                [os.path.join('glade', 'keystash.glade')] +
                glob(os.path.join('glade', '*.png'))
                )],
        classifiers = [
            'Development Status :: 4 - Beta',
            'Environment :: Console',
            'Environment :: X11 Applications :: GTK',
            'Intended Audience :: Developers',
            'Intended Audience :: System Administrators',
            'License :: OSI Approved :: GNU General Public License (GPL)',
            'Operating System :: POSIX',
            'Programming Language :: Python',
            'Topic :: Security',
            'Topic :: Utilities'
        ]
    )


if __name__ == '__main__':
    main()
